#include "midi.h"
#include "system.h"
#include <stdint.h>

/*
 * midi.c
 *
 *  Created on: Feb 20, 2016
 *      Author: Nick Felker
 */

#ifndef _MIDI_C_
#define _MIDI_C_

MIDI_tx_t transmitter_fnc;

void MIDI_Init(MIDI_tx_t transmitter) {
	transmitter_fnc = transmitter;
}
/**
 *
 */
void MIDI_Receive(uint8_t data) {
	//TODO Write some sort of receiver function pointer in the init, although we probably won't be using this anyway
}

void MIDI_SendNote(uint8_t channel, uint8_t note, uint8_t velocity) {
	char message[3];
	message[0] = 0x10 + channel;
	message[1] = note;
	message[2] = velocity;
	MIDI_SendMsg(message, 3);
}
void MIDI_SendNoteOff(uint8_t channel, uint8_t note) {
	//A noteoff event is basically a send on with 0 velocity
	MIDI_SendNote(channel, note, 0x00);
}
void MIDI_ChannelAftertouch(uint8_t channel, uint8_t velocity) {
	char message[3];
	message[0] = 0xD0 + channel - 0x80;
	message[1] = velocity;
	message[2] = velocity;
	MIDI_SendMsg(message, 3);
}
void MIDI_PolyphonicAftertouch(uint8_t channel, uint8_t note, uint8_t velocity) {
	char message[3];
	message[0] = 0xA0 + channel - 0x80;
	message[1] = note;
	message[2] = velocity;
	MIDI_SendMsg(message, 3);
}
void MIDI_Pitchbend(uint8_t channel, uint16_t value) {
	//The value should only be 14 bits long
	if(value > 0x4000) {
		value = 0x4000; //So we apply a maximum
	}
	char message[3];
	message[0] = 0xE0 + channel - 0x80;
	message[1] = value && 0x008F; //Get 7 LSB
	message[2] = value && 0x8F00; //Get 7 MSB
	MIDI_SendMsg((char*) message, 3);
}
void MIDI_ProgramChange(uint8_t channel, uint8_t newProgram) {
	char message[3];
	message[0] = 0xC0 + channel - 0x80;
	message[1] = newProgram;
	message[2] = 0x00;
}
void MIDI_Panic(uint8_t channel) {
	MIDI_SendChannelModeMsg(channel, 0x7B, 0);
}
void MIDI_Modulation(uint8_t channel, uint8_t modulation_value) {
	MIDI_SendChannelModeMsg(channel, 0x01, modulation_value);
}
void MIDI_DamperPedalOn(uint8_t channel) {
	MIDI_SendChannelModeMsg(channel, 0x40, 0x7F);
}
void MIDI_DamperPedalOff(uint8_t channel) {
	MIDI_SendChannelModeMsg(channel, 0x40, 0x00);
}
void MIDI_SetChannelVolume(uint8_t channel, uint8_t newVolume) {
	MIDI_SendChannelModeMsg(channel, 0x07, newVolume);
}
void MIDI_SetPan(uint8_t channel, uint8_t direction) {
	MIDI_SendChannelModeMsg(channel, 0x0A, direction);
}
void MIDI_PortamentoControl(uint8_t channel, uint8_t fromNote) {
	MIDI_SendChannelModeMsg(channel, 0x54, fromNote);
}
void MIDI_SequenceStart() {
	MIDI_SendSysRTMsg(0xFA);
}
void MIDI_SequenceStop() {
	MIDI_SendSysRTMsg(0xFC);
}
void MIDI_SequenceContinue() {
	MIDI_SendSysRTMsg(0xFB);
}
void MIDI_HardReset() {
	MIDI_SendSysRTMsg(0xFF);
}
void MIDI_SendChannelModeMsg(uint8_t channel, uint8_t msg, uint8_t value) {
	char message[3];
	message[0] = 0xB0 + channel - 0x80;
	message[1] = msg;
	message[2] = value;
	MIDI_SendMsg((char*) message, 3);
}
void MIDI_SendSysRTMsg(uint8_t msg) {
	char message[3];
	message[0] = msg - 0x80;
	message[1] = 0x00;
	message[2] = 0x00;
	MIDI_SendMsg((char*) message, 3);
}
void MIDI_SendMsg(uint8_t * data, uint8_t length) {
	transmitter_fnc(data, length);
	//Send some dummy data to flush UART buffer
	uint8_t dummy = 0;
	//transmitter_fnc(&dummy, 1);
}
#endif

