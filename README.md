# Embedded MIDI
This library can be used for an embedded system to create and send MIDI messages. It is written in C.

## What's Included?
`midi.c` and `midi.h` are the meat of the project. Importing them allows you to use the various MIDI functions.

### Initialize
To add MIDI to your project, do the following:
* Include a link to the midi.h header: `#include midi.h`
* Initialize the MIDI system and pass your transmitter function `MIDI_Init(fnc_ptr)`
* Implement your MIDI transmission: `void fnc_ptr(uint8_t * ptr, uint8_t len)`
    * Where `ptr` is an array of MIDI bytes and `len` is the length of the message (usually 3)
    * This function should send the message over UART, or Bluetooth, or whatever else you want

## Examples
There are example projects that you can use to test your project and see how this library has been used on other devices.

**NOTE**: Before you build the project in each example, change the `LIB_ROOT` variable to the location of your embedded library so the files will be found

### C for MSP430
This example project can be imported directly into Code Composer for the MSP430 and contains two different modes which can be toggled by switching the `MODE` constant.

* `MODE_LOOP` - This will constantly loop between `note_on` and `note_off` messages
* `MIDI_BUTTONS` - You can use the two buttons on the MSP430F5529 launchpad. Each one will toggle a specific music note 

### StateMachine
This project allows the user to loop through a variety of MIDI controller effects and hear the results by using the MSP430's two buttons.

Pressing the left buttons loops through: `TOGGLE_NOTE`, `TOGGLE_SUSTAIN_PEDAL`, `INCREASE_VOLUME`, `INCREASE_MODULATION`, `INCREASE_PAN`, `MIDI_PANIC`, `PORTAMENTO CONTROL`. When you're at that point, the right button completes that action.

## Where Can I Learn More?
To learn more about the MIDI standard and read specifications, check out these links:

* <a href='https://www.midi.org/specifications/item/table-1-summary-of-midi-message'>Summary of MIDI</a>
* <a href='http://oktopus.hu/uploaded/Tudastar/MIDI%201.0%20Detailed%20Specification.pdf'>Detailed Specification</a>