/*
 * File:   system.h
 * Author: Nick
 *
 * Created on March 8, 2015, 9:17 PM
 */

#ifndef SYSTEM_H_
#define SYSTEM_H_

#include <stdint.h>
#define FCPU 1048576 //1MHz

#include "hal_uart.h"
#include "hal_eeprom.h"
#include "hal_general.h"

#include "task.h"
#include "timing.h"
#include "uart.h"
#include "eeprom.h"
#include "buffer.h"
#include "charReceiverList.h"
#include "list.h"
#include "itemBuffer.h"

#include "keyfob_receiver.h"

//#define UART_BAUD 9600
#define USE_UART1
#define SUBSYS_UART 1

#define KEYFOB_SERIAL_BITS 20 // optional, default is 20
#define KEYFOB_INTERMEDIATE_BITS 0 // optional, default is 0
#define KEYFOB_DATA_BITS 4 // optional, default is 4
#define KEYFOB_POST_DATA_BITS 0 // optional, default is 0
#define KEYFOB_NUM_REMOTES 2 // optional, default is 2
#define KEYFOB_EE_BASE 0 // optioanl, default is 100

#define KEYFOB_MIN_TIME_UNIT 280
#define KEYFOB_MAX_TIME_UNIT 520

#define KEYFOB_PREAMBLE_UNITS 32

#define KEYFOB_CONSECUTIVE_SERIALS 2
#define KEYFOB_CONSECUTIVE_DATA 2
#define EEPROM_SIZE 20
#endif	/* SYSTEM_H */

