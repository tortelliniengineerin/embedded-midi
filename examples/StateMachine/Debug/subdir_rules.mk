################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Each subdirectory must supply rules for building sources it contributes
buffer.obj: C:/Users/guest1/Development/tortellini/embedded-library/src/buffer.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.6/bin/cl430" -vmspx --abi=eabi --data_model=restricted --use_hw_mpy=F5 --include_path="C:/ti/ccsv6/ccs_base/msp430/include" --include_path="C:/Users/guest1/Development/tortellini/embedded-library/include" --include_path="C:/Users/guest1/Development/tortellini/embedded-midi/examples/StateMachine" --include_path="C:/Users/guest1/Development/tortellini/embedded-library/hal/MSP430/MSP430F5529" --include_path="C:/Users/guest1/Development/tortellini/embedded-library/src" --include_path="C:/Users/guest1/Development/tortellini/embedded-library" --include_path="C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.6/include" --advice:power=all -g --define=__MSP430F5529__ --display_error_number --diag_wrap=off --diag_warning=225 --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU23 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="buffer.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

charReceiverList.obj: C:/Users/guest1/Development/tortellini/embedded-library/src/charReceiverList.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.6/bin/cl430" -vmspx --abi=eabi --data_model=restricted --use_hw_mpy=F5 --include_path="C:/ti/ccsv6/ccs_base/msp430/include" --include_path="C:/Users/guest1/Development/tortellini/embedded-library/include" --include_path="C:/Users/guest1/Development/tortellini/embedded-midi/examples/StateMachine" --include_path="C:/Users/guest1/Development/tortellini/embedded-library/hal/MSP430/MSP430F5529" --include_path="C:/Users/guest1/Development/tortellini/embedded-library/src" --include_path="C:/Users/guest1/Development/tortellini/embedded-library" --include_path="C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.6/include" --advice:power=all -g --define=__MSP430F5529__ --display_error_number --diag_wrap=off --diag_warning=225 --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU23 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="charReceiverList.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

hal_uart.obj: C:/Users/guest1/Development/tortellini/embedded-library/hal/MSP430/MSP430F5529/hal_uart.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.6/bin/cl430" -vmspx --abi=eabi --data_model=restricted --use_hw_mpy=F5 --include_path="C:/ti/ccsv6/ccs_base/msp430/include" --include_path="C:/Users/guest1/Development/tortellini/embedded-library/include" --include_path="C:/Users/guest1/Development/tortellini/embedded-midi/examples/StateMachine" --include_path="C:/Users/guest1/Development/tortellini/embedded-library/hal/MSP430/MSP430F5529" --include_path="C:/Users/guest1/Development/tortellini/embedded-library/src" --include_path="C:/Users/guest1/Development/tortellini/embedded-library" --include_path="C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.6/include" --advice:power=all -g --define=__MSP430F5529__ --display_error_number --diag_wrap=off --diag_warning=225 --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU23 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="hal_uart.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

itemBuffer.obj: C:/Users/guest1/Development/tortellini/embedded-library/src/itemBuffer.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.6/bin/cl430" -vmspx --abi=eabi --data_model=restricted --use_hw_mpy=F5 --include_path="C:/ti/ccsv6/ccs_base/msp430/include" --include_path="C:/Users/guest1/Development/tortellini/embedded-library/include" --include_path="C:/Users/guest1/Development/tortellini/embedded-midi/examples/StateMachine" --include_path="C:/Users/guest1/Development/tortellini/embedded-library/hal/MSP430/MSP430F5529" --include_path="C:/Users/guest1/Development/tortellini/embedded-library/src" --include_path="C:/Users/guest1/Development/tortellini/embedded-library" --include_path="C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.6/include" --advice:power=all -g --define=__MSP430F5529__ --display_error_number --diag_wrap=off --diag_warning=225 --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU23 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="itemBuffer.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

list.obj: C:/Users/guest1/Development/tortellini/embedded-library/src/list.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.6/bin/cl430" -vmspx --abi=eabi --data_model=restricted --use_hw_mpy=F5 --include_path="C:/ti/ccsv6/ccs_base/msp430/include" --include_path="C:/Users/guest1/Development/tortellini/embedded-library/include" --include_path="C:/Users/guest1/Development/tortellini/embedded-midi/examples/StateMachine" --include_path="C:/Users/guest1/Development/tortellini/embedded-library/hal/MSP430/MSP430F5529" --include_path="C:/Users/guest1/Development/tortellini/embedded-library/src" --include_path="C:/Users/guest1/Development/tortellini/embedded-library" --include_path="C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.6/include" --advice:power=all -g --define=__MSP430F5529__ --display_error_number --diag_wrap=off --diag_warning=225 --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU23 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="list.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

main.obj: ../main.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.6/bin/cl430" -vmspx --abi=eabi --data_model=restricted --use_hw_mpy=F5 --include_path="C:/ti/ccsv6/ccs_base/msp430/include" --include_path="C:/Users/guest1/Development/tortellini/embedded-library/include" --include_path="C:/Users/guest1/Development/tortellini/embedded-midi/examples/StateMachine" --include_path="C:/Users/guest1/Development/tortellini/embedded-library/hal/MSP430/MSP430F5529" --include_path="C:/Users/guest1/Development/tortellini/embedded-library/src" --include_path="C:/Users/guest1/Development/tortellini/embedded-library" --include_path="C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.6/include" --advice:power=all -g --define=__MSP430F5529__ --display_error_number --diag_wrap=off --diag_warning=225 --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU23 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="main.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

midi.obj: C:/Users/guest1/Development/tortellini/embedded-library/src/midi.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.6/bin/cl430" -vmspx --abi=eabi --data_model=restricted --use_hw_mpy=F5 --include_path="C:/ti/ccsv6/ccs_base/msp430/include" --include_path="C:/Users/guest1/Development/tortellini/embedded-library/include" --include_path="C:/Users/guest1/Development/tortellini/embedded-midi/examples/StateMachine" --include_path="C:/Users/guest1/Development/tortellini/embedded-library/hal/MSP430/MSP430F5529" --include_path="C:/Users/guest1/Development/tortellini/embedded-library/src" --include_path="C:/Users/guest1/Development/tortellini/embedded-library" --include_path="C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.6/include" --advice:power=all -g --define=__MSP430F5529__ --display_error_number --diag_wrap=off --diag_warning=225 --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU23 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="midi.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

task.obj: C:/Users/guest1/Development/tortellini/embedded-library/src/task.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.6/bin/cl430" -vmspx --abi=eabi --data_model=restricted --use_hw_mpy=F5 --include_path="C:/ti/ccsv6/ccs_base/msp430/include" --include_path="C:/Users/guest1/Development/tortellini/embedded-library/include" --include_path="C:/Users/guest1/Development/tortellini/embedded-midi/examples/StateMachine" --include_path="C:/Users/guest1/Development/tortellini/embedded-library/hal/MSP430/MSP430F5529" --include_path="C:/Users/guest1/Development/tortellini/embedded-library/src" --include_path="C:/Users/guest1/Development/tortellini/embedded-library" --include_path="C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.6/include" --advice:power=all -g --define=__MSP430F5529__ --display_error_number --diag_wrap=off --diag_warning=225 --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU23 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="task.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

timing.obj: C:/Users/guest1/Development/tortellini/embedded-library/src/timing.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.6/bin/cl430" -vmspx --abi=eabi --data_model=restricted --use_hw_mpy=F5 --include_path="C:/ti/ccsv6/ccs_base/msp430/include" --include_path="C:/Users/guest1/Development/tortellini/embedded-library/include" --include_path="C:/Users/guest1/Development/tortellini/embedded-midi/examples/StateMachine" --include_path="C:/Users/guest1/Development/tortellini/embedded-library/hal/MSP430/MSP430F5529" --include_path="C:/Users/guest1/Development/tortellini/embedded-library/src" --include_path="C:/Users/guest1/Development/tortellini/embedded-library" --include_path="C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.6/include" --advice:power=all -g --define=__MSP430F5529__ --display_error_number --diag_wrap=off --diag_warning=225 --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU23 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="timing.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

uart.obj: C:/Users/guest1/Development/tortellini/embedded-library/src/uart.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.6/bin/cl430" -vmspx --abi=eabi --data_model=restricted --use_hw_mpy=F5 --include_path="C:/ti/ccsv6/ccs_base/msp430/include" --include_path="C:/Users/guest1/Development/tortellini/embedded-library/include" --include_path="C:/Users/guest1/Development/tortellini/embedded-midi/examples/StateMachine" --include_path="C:/Users/guest1/Development/tortellini/embedded-library/hal/MSP430/MSP430F5529" --include_path="C:/Users/guest1/Development/tortellini/embedded-library/src" --include_path="C:/Users/guest1/Development/tortellini/embedded-library" --include_path="C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.6/include" --advice:power=all -g --define=__MSP430F5529__ --display_error_number --diag_wrap=off --diag_warning=225 --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU23 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="uart.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '


