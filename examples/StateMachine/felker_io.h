#include <msp430f5529.h>

#ifndef FELKER_IO_H
#define FELKER_IO_H

typedef enum { false, true } bool;
typedef enum { LOW, HIGH } digitalio;

/**
 * States that the pin is an output
 */
#define OUTPUT 1
/**
 * States that the pin is an input
 */
#define INPUT 2
/**
 * States that the pin is not for general input or output
 */
#define PERIPHERAL 3
/**
 * States that the pin is an input and has a pullup resistor
 */
#define INPUT_PULLUP 4

/**
 * Sets the given pin direction and its selection value
 * @param port The port value, P1 - P4
 * @param bit The pin number 0-7
 * @param mode The type of pin mode, ie. INPUT or OUTPUT
 */
void pinMode(volatile unsigned int port, unsigned int bit, unsigned int mode) {
	if(mode == OUTPUT) {
		if(port == 1)
			P1DIR |= bit;
		else if(port == 2)
			P2DIR |= bit;
		else if(port == 3)
			P3DIR |= bit;
		else if(port == 4)
			P4DIR |= bit;
	} else if(mode == INPUT || mode == INPUT_PULLUP) {
		if(port == 1)
			P1DIR &= ~bit;
		else if(port == 2)
			P2DIR &= ~bit;
		else if(port == 3)
			P3DIR &= ~bit;
		else if(port == 4)
			P4DIR &= ~bit;
	}
	if(mode == INPUT_PULLUP) {
		if(port == 1) {
			P1REN |= bit; // Enable resistor on P1.1
			P1OUT = bit; // Set resistor to pull-up, P1.0 low
		} else if(port == 2) {
			P2REN |= bit; // Enable resistor on P1.1
			P2OUT = bit; // Set resistor to pull-up, P1.0 low
		} else if(port == 3) {
			P3REN |= bit; // Enable resistor on P1.1
			P3OUT = bit; // Set resistor to pull-up, P1.0 low
		} else if(port == 4) {
			P4REN |= bit; // Enable resistor on P1.1
			P4OUT = bit; // Set resistor to pull-up, P1.0 low
		}
	}

	if(mode == OUTPUT || mode == INPUT || mode == INPUT_PULLUP) {
		//It's an I/O object, set PxSEL to 0
		if(port == 1)
			P1SEL &= ~bit;
		else if(port == 2)
			P2SEL &= ~bit;
		else if(port == 3)
			P3SEL &= ~bit;
		else if(port == 4)
			P4SEL &= ~bit;
	} else if(mode == PERIPHERAL) {
		//Set PxSEL to 1 for peripherals
		if(port == 1)
			P1SEL |= bit;
		else if(port == 2)
			P2SEL |= bit;
		else if(port == 3)
			P3SEL |= bit;
		else if(port == 4)
			P4SEL |= bit;
	}
}
/**
 * Changes the value of the pin from HIGH to LOW or vice versa if it is an output pin.
 * @param port The port value, P1 - P4
 * @param bit The pin number 0-7
 */
void togglePinValue(volatile unsigned int port, unsigned int bit) {
	if(port == 1)
		P1OUT ^= bit;
	else if(port == 2)
		P2OUT ^= bit;
	else if(port == 3)
		P3OUT ^= bit;
	else if(port == 4)
		P4OUT ^= bit;
}
void digitalWrite(volatile unsigned int port, unsigned int bit, unsigned char val) {
	if(port == 1 && val == HIGH)
		P1OUT |= bit;
	if(port == 1 && val == LOW)
		P1OUT &= ~bit;
	if(port == 2 && val == HIGH)
		P2OUT |= bit;
	if(port == 2 && val == LOW)
		P2OUT &= ~bit;
	if(port == 3 && val == HIGH)
		P3OUT |= bit;
	if(port == 3 && val == LOW)
		P3OUT &= ~bit;
	if(port == 4 && val == HIGH)
		P4OUT |= bit;
	if(port == 4 && val == LOW)
		P4OUT &= ~bit;
}

digitalio digitalRead(volatile unsigned int port, unsigned int bit) {
	if(port == 1 && P1IN & bit)
		return HIGH;
	else if(port == 1)
		return LOW;
	else if(port == 2 && P2IN & bit)
		return HIGH;
	else if(port == 2)
		return LOW;
	else if(port == 3 && P3IN & bit)
		return HIGH;
	else if(port == 3)
		return LOW;
	else if(port == 4 && P4IN & bit)
		return HIGH;
	else if(port == 4)
		return LOW;
}

typedef struct {
	volatile unsigned int uart_id;
	volatile unsigned int port;
	unsigned int tx;
	unsigned int rx;
} uartPin;

typedef struct {
	void (*onReceived) (char);
} uartCallback;

/**
 * The GPIO struct makes it easy to get pins and pin values
 */
typedef struct {
	volatile unsigned int p1;
	volatile unsigned int p2;
	volatile unsigned int p3;
	volatile unsigned int p4;

	unsigned int b0;
	unsigned int b1;
	unsigned int b2;
	unsigned int b3;
	unsigned int b4;
	unsigned int b5;
	unsigned int b6;
	unsigned int b7;

	uartPin uart0;
	uartPin uart1;
} gpio;

/**
 * Stops the watchdog timer. Is called in the superMain function
 */
void stopWatchdogTimer() {
	WDTCTL = WDTPW | WDTHOLD;	// Stop watchdog timer
}

/**
 * Sends a no operation command, usually for debug purposes
 */
void nopIt() {
	__no_operation();
}

/**
 * Enters low power mode. Normal code will not run, but interrupts may work
 */
void enterLowPowerMode() {
	_BIS_SR(LPM0_bits + GIE);//This line of code calls the _BIS_SR function which is used to set the processors operating mode
}

/* Let's declare our uart function pointers */
uartCallback uartCallback1;

/**
 * Inits a uart pin with a 9600 baud rate
 * @param uart A pointer to the given uartPin struct you want to initialize
 * @param onreceive The function that would be called when you receive data
 */
void initUart(uartPin *uart, void (*onreceive)(char)) {
	pinMode(uart->port, uart->tx, PERIPHERAL);
	pinMode(uart->port, uart->rx, PERIPHERAL);
	if(uart->uart_id == 1) {
		 // P4SEL = BIT4+BIT5; //TODO I might want this to be better integrated
		  UCA1CTL1 |= UCSWRST;                      // **Put state machine in reset**
		  UCA1CTL1 |= UCSSEL_2;                     // SMCLK
		  UCA1BR0 = 6;                              // 1MHz 9600 (see User's Guide)
		  UCA1BR1 = 0;                              // 1MHz 9600
		  UCA1MCTL = UCBRS_0 + UCBRF_13 + UCOS16;   // Modln UCBRSx=0, UCBRFx=0,
		                                            // over sampling
		  UCA1CTL1 &= ~UCSWRST;                     // **Initialize USCI state machine**
		  UCA1IE |= UCRXIE;                         // Enable USCI_A0 RX interrupt
		  uartCallback1.onReceived = onreceive;
	}
}

/**
 * Sends a character to a UART buffer to be transfered
 * @param uart The uartPin struct that data will be written to
 * @param c The character that will be transferred
 */
void uartTransfer(uartPin *uart, char c) {
	if(uart->uart_id == 1) {
		UCA1TXBUF = c;
	}
}

/**
 * Sends a character to a UART buffer to be transfered
 * @param uart The uartPin struct that data will be written to
 * @param c The character that will be transferred
 */
void uartUnsignedTransfer(uartPin *uart, unsigned char c) {
	if(uart->uart_id == 1) {
		UCA1TXBUF = c;
	}
}

bool uartBufferReady(int uartPin) {
	if(uartPin == 1) {
		if(UCA1IFG&UCTXIFG) {
			return true;
		} else {
			return false;
		}
	} else {
		return false;
	}
}

/**
 * This should be the first function called. It will call initialization functions.
 * @return A GPIO object which you can use to do pin inits
 */
gpio superMain() {
	//Do any important initialization stuff
	stopWatchdogTimer();

	//Create our GPIO object
	gpio newgpio;
//	P1SEL &= 0x0000;
	newgpio.p1 = 1;
	newgpio.p2 = 2;
	newgpio.p3 = 3;
	newgpio.p4 = 4;

	newgpio.b0 = BIT0;
	newgpio.b1 = BIT1;
	newgpio.b2 = BIT2;
	newgpio.b3 = BIT3;
	newgpio.b4 = BIT4;
	newgpio.b5 = BIT5;
	newgpio.b6 = BIT6;
	newgpio.b7 = BIT7;

	newgpio.uart1.uart_id = 1;
	newgpio.uart1.port = 4;
	newgpio.uart1.tx = BIT4;
	newgpio.uart1.rx = BIT5;

	return newgpio;
}
/*
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=USCI_A1_VECTOR
__interrupt void USCI_A1_ISR(void)
#elif defined(__GNUC__)
void __attribute__ ((interrupt(USCI_A1_VECTOR))) USCI_A1_ISR (void)
#else
#error Compiler not supported!
#endif
{
  switch(__even_in_range(UCA1IV,4))
  {
  case 0:break;                             // Vector 0 - no interrupt
  case 2:                                   // Vector 2 - RXIFG
	  while (uartBufferReady(1) == false);             // USCI_A0 TX buffer ready?
	  uartCallback1.onReceived(UCA1RXBUF);
    break;
  case 4:break;                             // Vector 4 - TXIFG
  default: break;
  }
}
*/
#endif
