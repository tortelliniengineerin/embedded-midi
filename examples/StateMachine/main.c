#include "felker_io.h"
#include "midi.h"
#include "system.h"
#include <stdint.h>

/*
 * main.c
 */
gpio msp;
char STATE_COUNT = 8;
char currentState = 0;
enum MIDI_STATES { NOTE, PEDAL, VOLUME, MODULATION, PAN, PANIC, PORTAMENTO, SONG };

unsigned int i;
void sendMIDIOverUart(uint8_t * ptr, uint8_t len) {
/*	volatile unsigned char smii = 0;
	volatile unsigned int smiii = 0;
	for(smii=0;smii<len;smii++) {
		char byte = (char) (ptr[smii]);
		UART_WriteByte(1, byte);
		for(smiii=0;smiii<500;smiii++) {}
	}*/
	UART_Write(1, ptr, len);
}
volatile char note = 70;
bool noteOn = false;
bool pedal = false;
volatile char volume = 127;
volatile char modulation = 0;
volatile char pan = 63;
void NoteOn() {
	MIDI_SendNote(0, note, 127);
	digitalWrite(msp.p4, msp.b7, HIGH);
}

void NoteOff() {
	MIDI_SendNoteOff(0, note);
	digitalWrite(msp.p4, msp.b7, LOW);
}

void togglePedal() {
	if(pedal == true) {
		MIDI_DamperPedalOff(0);
		digitalWrite(msp.p1, msp.b0, LOW);
		pedal = false;
	} else {
		MIDI_DamperPedalOn(0);
		pedal = true;
		digitalWrite(msp.p1, msp.b0, HIGH);
	}
}
void increaseVolume() {
	volume += 16;
	if(volume > 127) {
		volume = 0;
	}
	MIDI_SetChannelVolume(0, volume);
}
void increaseModulation() {
	modulation += 16;
	if(modulation > 127) {
		modulation = 0;
	}
	MIDI_Modulation(0, modulation);
}
void increasePan() {
	pan += 16;
	if(pan > 127) {
		pan = 0;
	}
	MIDI_SetPan(0, pan);
}
void panic() {
	MIDI_Panic(0);
}
void portamento() {
	MIDI_PortamentoControl(0, note);
	note += 6;
	if(note > 96)
		note = 70;
	MIDI_SendNote(0, note, 127);
}

#define C 0
#define Cs 1
#define D 2
#define Ds 3
#define E 4
#define F 5
#define Fs 6
#define G 7
#define Gs 8
#define A 9
#define As 10
#define B 11
#define Bb -1

#define SONG_2
#ifdef SONG_1
#define SONG_LENGTH 20
int8_t song_data[SONG_LENGTH] = { G, C, C, G, C, C, G, C, Gs, C, F, Bb, Bb, F, Bb, Bb, F, Bb, Fs, Bb};
#define SONG_OCTIVE 4
#elif defined SONG_2
#define SONG_LENGTH 26
int8_t song_data[SONG_LENGTH] = { E, D, C, D, E, E, E, D, D, D, E, G, G, E, D, C, D, E, E, E, E, D, D, E, D, C};
#define SONG_OCTIVE 4
#endif


void song(uint8_t on) {
	static uint8_t i = SONG_LENGTH-1;
	uint8_t n;
	n = song_data[i] + (SONG_OCTIVE+2)*12;
	if(on) {
		digitalWrite(msp.p4, msp.b7, HIGH);
		MIDI_SendNote(0, n, 127);
	}else {
		digitalWrite(msp.p4, msp.b7, LOW);
		MIDI_SendNoteOff(0, n);
		i++;
		if(i >= SONG_LENGTH) i = 0;
	}
}

bool button1 = false;
bool button2 = false;
int main(void) {
	msp = superMain();
    Timing_Init();
    Task_Init();

    UART_Init(1);
//    initUart(&(msp.uart1), &printChar);
    MIDI_Init(sendMIDIOverUart);

//    initUart(&(msp.uart1), &printChar);
    pinMode(msp.p1, msp.b0, OUTPUT);
    pinMode(msp.p4, msp.b7, OUTPUT);
	pinMode(msp.p1, msp.b1, INPUT_PULLUP);
	pinMode(msp.p2, msp.b1, INPUT_PULLUP);
	digitalWrite(msp.p1, msp.b0, LOW);
	digitalWrite(msp.p4, msp.b7, LOW);
	volatile unsigned int index = 0;

	volatile unsigned step = 0;
	EnableInterrupts();
    while(true) {
    	if(digitalRead(msp.p2, msp.b1) == LOW) {
			while(digitalRead(msp.p2, msp.b1) == LOW) { }
			currentState++;
			if(currentState >= STATE_COUNT)
				currentState = 0;
		}
    	if(digitalRead(msp.p1, msp.b1) == LOW) {
    		// button down action
    		switch(currentState) {
				case NOTE:
					NoteOn();
					break;
				case SONG:
					song(1);
					break;
			}
			while(digitalRead(msp.p1, msp.b1) == LOW) { }
			// button release action
			switch(currentState) {
			case NOTE:
				NoteOff();
				break;
			case PEDAL:
				togglePedal();
				break;
			case VOLUME:
				increaseVolume();
				break;
			case MODULATION:
				increaseModulation();
				break;
			case PAN:
				increasePan();
				break;
			case PANIC:
				panic();
				break;
			case PORTAMENTO:
				portamento();
				break;
			case SONG:
				song(0);
				break;
			}
		}
    	/*if(step == 4096) {
    		step = 0;
    	}
    	step++;*/
    	SystemTick();
    }
}

