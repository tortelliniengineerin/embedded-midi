#include "felker_io.h"
#include "felker_io_uart.h"

#ifndef FELKER_IO_H
#error "You need to #include felker_io.h"
#endif

void pinMode(volatile unsigned int port, unsigned int bit, unsigned int mode) {
	if(mode == OUTPUT) {
		if(port == 1)
			P1DIR |= bit;
		else if(port == 2)
			P2DIR |= bit;
		else if(port == 3)
			P3DIR |= bit;
		else if(port == 4)
			P4DIR |= bit;
	} else if(mode == INPUT || mode == INPUT_PULLUP) {
		if(port == 1)
			P1DIR &= ~bit;
		else if(port == 2)
			P2DIR &= ~bit;
		else if(port == 3)
			P3DIR &= ~bit;
		else if(port == 4)
			P4DIR &= ~bit;
	}
	if(mode == INPUT_PULLUP) {
		if(port == 1) {
			P1REN |= bit; // Enable resistor on P1.1
			P1OUT = bit; // Set resistor to pull-up, P1.0 low
		} else if(port == 2) {
			P2REN |= bit; // Enable resistor on P1.1
			P2OUT = bit; // Set resistor to pull-up, P1.0 low
		} else if(port == 3) {
			P3REN |= bit; // Enable resistor on P1.1
			P3OUT = bit; // Set resistor to pull-up, P1.0 low
		} else if(port == 4) {
			P4REN |= bit; // Enable resistor on P1.1
			P4OUT = bit; // Set resistor to pull-up, P1.0 low
		}
	}

	if(mode == OUTPUT || mode == INPUT || mode == INPUT_PULLUP) {
		//It's an I/O object, set PxSEL to 0
		if(port == 1)
			P1SEL &= ~bit;
		else if(port == 2)
			P2SEL &= ~bit;
		else if(port == 3)
			P3SEL &= ~bit;
		else if(port == 4)
			P4SEL &= ~bit;
	} else if(mode == PERIPHERAL) {
		//Set PxSEL to 1 for peripherals
		if(port == 1)
			P1SEL |= bit;
		else if(port == 2)
			P2SEL |= bit;
		else if(port == 3)
			P3SEL |= bit;
		else if(port == 4)
			P4SEL |= bit;
	}
}

void togglePinValue(volatile unsigned int port, unsigned int bit) {
	if(port == 1)
		P1OUT ^= bit;
	else if(port == 2)
		P2OUT ^= bit;
	else if(port == 3)
		P3OUT ^= bit;
	else if(port == 4)
		P4OUT ^= bit;
}

void digitalWrite(volatile unsigned int port, unsigned int bit, unsigned char val) {
	if(port == 1 && val == HIGH)
		P1OUT |= bit;
	if(port == 1 && val == LOW)
		P1OUT &= ~bit;
	if(port == 2 && val == HIGH)
		P2OUT |= bit;
	if(port == 2 && val == LOW)
		P2OUT &= ~bit;
	if(port == 3 && val == HIGH)
		P3OUT |= bit;
	if(port == 3 && val == LOW)
		P3OUT &= ~bit;
	if(port == 4 && val == HIGH)
		P4OUT |= bit;
	if(port == 4 && val == LOW)
		P4OUT &= ~bit;
}

digitalio digitalRead(volatile unsigned int port, unsigned int bit) {
	if(port == 1 && P1IN & bit)
		return HIGH;
	else if(port == 1)
		return LOW;
	else if(port == 2 && P2IN & bit)
		return HIGH;
	else if(port == 2)
		return LOW;
	else if(port == 3 && P3IN & bit)
		return HIGH;
	else if(port == 3)
		return LOW;
	else if(port == 4 && P4IN & bit)
		return HIGH;
	else if(port == 4)
		return LOW;
	else
		return LOW;
}

void stopWatchdogTimer() {
	WDTCTL = WDTPW | WDTHOLD;	// Stop watchdog timer
}

void nopIt() {
	__no_operation();
}

void enterLowPowerMode() {
	_BIS_SR(LPM0_bits + GIE);//This line of code calls the _BIS_SR function which is used to set the processors operating mode
}

/**
 * This should be the first function called. It will call initialization functions.
 * @return A GPIO object which you can use to do pin inits
 */
microcontroller getMicroController() {
	//Do any important initialization stuff
	stopWatchdogTimer();

	//Create our GPIO object
	microcontroller newgpio;
	newgpio.p1 = 1;
	newgpio.p2 = 2;
	newgpio.p3 = 3;
	newgpio.p4 = 4;

	newgpio.b0 = BIT0;
	newgpio.b1 = BIT1;
	newgpio.b2 = BIT2;
	newgpio.b3 = BIT3;
	newgpio.b4 = BIT4;
	newgpio.b5 = BIT5;
	newgpio.b6 = BIT6;
	newgpio.b7 = BIT7;

	newgpio.uart1.uart_id = 1;
	newgpio.uart1.port_id = 4;
	newgpio.uart1.tx = BIT4;
	newgpio.uart1.rx = BIT5;

	return newgpio;
}
