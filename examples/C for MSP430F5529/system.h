/*
 * File:   system.h
 * Author: Nick
 *
 * Created on March 8, 2015, 9:17 PM
 */

#ifndef C_FOR_MSP430_SYSTEM_H_
#define C_FOR_MSP430_SYSTEM_H_

#include <stdint.h>
#define FCPU 1048576//1000000// //1MHz

//#include "library.h"

#include "hal_uart.h"
#include "hal_general.h"

#include "task.h"
#include "timing.h"
#include "uart.h"
#include "buffer.h"
#include "charReceiverList.h"
#include "list.h"
#include "itemBuffer.h"

#define UART_BAUD 9600
#define USE_UART1

#endif	/* SYSTEM_H */

