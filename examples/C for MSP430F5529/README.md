# C for MSP430
This project is designed for use with the MSP430F5529. If you don't have that, some of the libraries will not work correctly and that will cause the build to fail.

## Setup
To set up this project, first you must modify the Code Composer variable `LIB_ROOT` to point to the location of Mulhbaier's embedded library.

Then flash it to your microcontroller. To test it out, press the buttons. Each one will toggle a particular note.

To receive data, you should use the Tortellini MIDI receiever.

https://bitbucket.org/tortelliniengineerin/tortellini-midi-receiver