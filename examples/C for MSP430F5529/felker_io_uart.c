/*
 * felker_io_uart.c
 *
 *  Created on: Mar 8, 2016
 *      Author: guest1
 */

#include "felker_io.h"
#include "felker_io_uart.h"

void initUart(uartPin *uart, void (*onreceive)(char)) {
	pinMode(uart->port_id, uart->tx, PERIPHERAL);
	pinMode(uart->port_id, uart->rx, PERIPHERAL);
	if(uart->uart_id == 1) {
		 // P4SEL = BIT4+BIT5; //TODO I might want this to be better integrated
		  UCA1CTL1 |= UCSWRST;                      // **Put state machine in reset**
		  UCA1CTL1 |= UCSSEL_2;                     // SMCLK
		  UCA1BR0 = 6;                              // 1MHz 9600 (see User's Guide)
		  UCA1BR1 = 0;                              // 1MHz 9600
		  UCA1MCTL = UCBRS_0 + UCBRF_13 + UCOS16;   // Modln UCBRSx=0, UCBRFx=0,
		                                            // over sampling
		  UCA1CTL1 &= ~UCSWRST;                     // **Initialize USCI state machine**
		  UCA1IE |= UCRXIE;                         // Enable USCI_A0 RX interrupt
		  uartCallback1.onReceived = onreceive;
	}
}

void uartTransfer(uartPin *uart, char c) {
	if(uart->uart_id == 1) {
		UCA1TXBUF = c;
	}
}

void uartUnsignedTransfer(uartPin *uart, unsigned char c) {
	if(uart->uart_id == 1) {
		UCA1TXBUF = c;
	}
}

bool uartBufferReady(int uartPin) {
	if(uartPin == 1) {
		if(UCA1IFG&UCTXIFG) {
			return true;
		} else {
			return false;
		}
	} else {
		return false;
	}
}
