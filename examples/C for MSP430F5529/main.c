#include "felker_io.h"
#include "midi.h"
#include "system.h"
#include <stdint.h>

/*
 * main.c
 */
#define MODE_LOOP 1
#define MODE_BUTTONS 2
#define MODE MODE_BUTTONS
microcontroller msp;
void printChar(char rx) {
	//uartTransfer(&(msp430f5529.uart1), rx);
	togglePinValue(msp.p1, msp.b0);
}

unsigned int i;
void sendMIDIOverUart(uint8_t * ptr, uint8_t len) {
	/*volatile unsigned char smii = 0;
	volatile unsigned int smiii = 0;
	for(smii=0;smii<len;smii++) {
		char byte = (char) (ptr[smii]);
		UART_WriteByte(1, byte);
		for(smiii=0;smiii<150;smiii++) {}
	}*/
	UART_Write(1, ptr, len);
}

bool button1 = false;
bool button2 = false;
int main(void) {
	msp = getMicroController();


/*
	//clock configuration- Mulby's style, but changed to 20Mhz because that is what my User guide has values for the UART for
	    //this clock is to make the 115200 Baud work well... i think. This code works (i believe) and
	    void SetVcoreUp (unsigned int level);
	    //...
	    //the following would typically be located at the top of main
	    // Increase Vcore setting to level3 to support fsystem=25MHz ///// not sure if I need to change that for 20MHz
	    // NOTE: Change core voltage one level at a time..
//	    SetVcoreUp (0x01);
	//    SetVcoreUp (0x02);
//	    SetVcoreUp (0x03);
	    UCSCTL3 = SELREF_2;                       // Set DCO FLL reference = REFO
	    UCSCTL4 |= SELA_2;                        // Set ACLK = REFO
	    __bis_SR_register(SCG0);                  // Disable the FLL control loop
	    UCSCTL0 = 0x0000;                         // Set lowest possible DCOx, MODx
	    UCSCTL1 = DCORSEL_7;                      // Select DCO range 50MHz operation
	    UCSCTL2 = FLLD_0 + 31;                   // Set DCO Multiplier for 20MHz
	                                            // (N + 1) * FLLRef = Fdco
	                                            // (761 + 1) * 32768 = 25 MHz
	                                            // Set FLL Div = fDCOCLK/2
	    __bic_SR_register(SCG0);                  // Enable the FLL control loop
	    // Worst-case settling time for the DCO when the DCO range bits have been
	    // changed is n x 32 x 32 x f_MCLK / f_FLL_reference. See UCS chapter in 5xx
	    // UG for optimization.
	    // 32 x 32 x 25 MHz / 32,768 Hz ~ 780k MCLK cycles for DCO to settle
	    __delay_cycles(782000);
	    // Loop until XT1,XT2 & DCO stabilizes - In this case only DCO has to stabilize
//	    do
//	    {
//	    UCSCTL7 &= ~(XT2OFFG + XT1LFOFFG + DCOFFG);
//	                                            // Clear XT2,XT1,DCO fault flags
//	    SFRIFG1 &= ~OFIFG;                      // Clear fault flags
//	    }while (SFRIFG1&OFIFG);                   // Test oscillator fault flag
	    ////////////////////////////////////// clock config end



*/

    Timing_Init();
    Task_Init();

    UART_Init(1);
//    initUart(&(msp.uart1), &printChar);
    MIDI_Init(sendMIDIOverUart);

//    initUart(&(msp.uart1), &printChar);
    pinMode(msp.p1, msp.b0, OUTPUT);
    pinMode(msp.p4, msp.b7, OUTPUT);
	pinMode(msp.p1, msp.b1, INPUT_PULLUP);
	pinMode(msp.p2, msp.b1, INPUT_PULLUP);
	digitalWrite(msp.p1, msp.b0, LOW);
	digitalWrite(msp.p4, msp.b7, LOW);
	volatile unsigned int index = 0;
	//Task_SetIdleTask(UART_Tick);
	EnableInterrupts();

#if MODE == MODE_LOOP
	volatile unsigned int speed = 5000;
	digitalWrite(msp.p4, msp.b7, HIGH);
	while(true) {
		MIDI_SendNote(0, 70, 127);
		togglePinValue(msp.p1, msp.b0);
		togglePinValue(msp.p4, msp.b7);
		for(i=0;i<speed;i++) {
			SystemTick();
		}
		MIDI_SendNoteOff(0, 70);
		togglePinValue(msp.p1, msp.b0);
		togglePinValue(msp.p4, msp.b7);
		for(i=0;i<speed;i++) {
			SystemTick();
		}
		if(!(P2IN & BIT1)) {
			while(!(P2IN & BIT1)) { }
			speed -= 500;
		}
		if(!(P1IN & BIT1)) {
			while(!(P1IN & BIT1)) { }
			speed += 500;
		}
	}
#elif MODE == MODE_BUTTONS
	volatile unsigned step = 0;
    while(true) {
    	if(digitalRead(msp.p2, msp.b1) == LOW) {
			while(digitalRead(msp.p2, msp.b1) == LOW) {

			}
			if(button2 == true) {
				MIDI_SendNoteOff(0, 70);
				digitalWrite(msp.p1, msp.b0, LOW);
				button2 = false;
			} else {
				MIDI_SendNote(0, 70, 127);
				digitalWrite(msp.p1, msp.b0, HIGH);
				button2 = true;
			}
		}
    	if(digitalRead(msp.p1, msp.b1) == LOW) {
			while(digitalRead(msp.p1, msp.b1) == LOW) { }
			if(button1 == false) {
				MIDI_SendNote(0, 76, 127);
				button1 = true;
				digitalWrite(msp.p4, msp.b7, HIGH);
			} else {
				MIDI_SendNoteOff(0, 76);
				digitalWrite(msp.p4, msp.b7, LOW);
				button1 = false;
			}
		}
    	if(step == 4096) {
    		//_no_operation();
    		//MIDI_Panic(1);
    		step = 0;
    	}
    	step++;
    	SystemTick();
    }
#endif
}

void SetVcoreUp (unsigned int level)
{
  // Open PMM registers for write
  PMMCTL0_H = PMMPW_H;
  // Set SVS/SVM high side new level
  SVSMHCTL = SVSHE + SVSHRVL0 * level + SVMHE + SVSMHRRL0 * level;
  // Set SVM low side to new level
  SVSMLCTL = SVSLE + SVMLE + SVSMLRRL0 * level;
  // Wait till SVM is settled
  while ((PMMIFG & SVSMLDLYIFG) == 0);
  // Clear already set flags
  PMMIFG &= ~(SVMLVLRIFG + SVMLIFG);
  // Set VCore to new level
  PMMCTL0_L = PMMCOREV0 * level;
  // Wait till new level reached
  if ((PMMIFG & SVMLIFG))
    while ((PMMIFG & SVMLVLRIFG) == 0);
  // Set SVS/SVM low side to new level
  SVSMLCTL = SVSLE + SVSLRVL0 * level + SVMLE + SVSMLRRL0 * level;
  // Lock PMM registers for write access
  PMMCTL0_H = 0x00;
}
